---
layout: post
title: itemized receipt
category: posts
---

*Icy veins Diablo 3 gear scraper*

I needed an easy way to tell if an item fits my build.

Features a local database builder on a Tkinter app.

Take a look at the project if you want:
[itemized receipt][itemized receipt]

---

[discord][discord]

[github][dqd]

[PGP][PGP]

[coffee][coffee]

[discord]: https://discord.com/channels/@me/dqd#0143
[dqd]: https://github.com/dqdang
[PGP]: https://raw.githubusercontent.com/dqdang/dqdang.github.io/master/derek-dang.asc
[coffee]: https://www.buymeacoffee.com/dqdang
[itemized receipt]: https://github.com/dqdang/itemized-receipt
